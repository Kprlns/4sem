#include <algorithm>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <vector>

const int N = 4;

inline void PrintValueAndBits(int value, const std::string& name) {
    std::cout << name << ": " << value << ", bits: ";
    int currentPower = 1 << (N - 1);
    while (currentPower) {
        std::cout << (value & currentPower ? '1' : '0');
        currentPower >>= 1;
    }
    std::cout << std::endl;
}

std::vector<char> GetLetters(const std::string& text) {
    std::set<char> tmp;
    for (size_t idx = 0; idx < text.size(); ++idx) {
        tmp.insert(text[idx]);
    }
    tmp.insert(std::numeric_limits<char>::max());
    std::vector<char> result;
    for (const auto& item : tmp) {
        result.push_back(item);
    }
    return result;
}

int GetPosition(const std::vector<char>& letters, const char letter) {
    return std::find(letters.begin(), letters.end(), letter) - letters.begin();
}

std::string EncodeSymbol(const int pos, const std::vector<int>& cumFreq, int& l, int& h, int& waitingBit) {
    int lNew = l + (h - l + 1) * cumFreq[pos + 1] / cumFreq[0];
    int hNew = l + (h - l + 1) * cumFreq[pos] / cumFreq[0] - 1;
    l = lNew;
    h = hNew;
    PrintValueAndBits(l, "l");
    PrintValueAndBits(h, "h");
    int lastBit = 1 << (N - 1);
    std::stringstream result;
    while (((h & lastBit) == (l & lastBit)) || (h - l < cumFreq[0])) {
        if ((h & lastBit) == (l & lastBit)) {
            int resultBit = (h & lastBit) > 0 ? 1 : 0;
            result << resultBit;
            while (waitingBit) {
                result << 1 - resultBit;
                --waitingBit;
            }
            if (l >= lastBit) {
                l -= lastBit;
                h -= lastBit;
            }
            l = l << 1;
            h = (h << 1) + 1;
            PrintValueAndBits(l, "Update l");
            PrintValueAndBits(h, "Update h");
            std::cout << "Current code: " << result.str() << std::endl;
        } else {
            std::cout << "Waiting bit" << std::endl;
            ++waitingBit;
            int value = 1 << (N - 2);
            l = 2 * (l - value);
            h = 2 * (h - value) + 1;
            PrintValueAndBits(l, "Update l");
            PrintValueAndBits(h, "Update h");
        }
    }
    std::cout << "Final code: " << result.str() << std::endl;
    return result.str();
}

std::string Encode(const std::string& text, const std::vector<char>& letters) {
    int l = 0;
    int h = (1 << N) - 1;
    PrintValueAndBits(l, "Starting value of l");
    PrintValueAndBits(h, "Starting value of h");
    std::cout << std::endl;
    std::vector<int> cumFreq(letters.size() + 1);
    std::string result;
    int waitingBit = 0;

    for (size_t idx = 0; idx < cumFreq.size(); ++idx) {
        cumFreq[idx] = cumFreq.size() - 1 - idx;
    }

    for (size_t idx = 0; idx < text.size(); ++idx) {
        std::cout << "Encoding symbol: " << text[idx] << std::endl;
        std::cout << "Cummulative frequencies:";
        for (const int value : cumFreq) {
            std::cout << " " << value;
        }
        std::cout << std::endl << "WB " << waitingBit << std::endl;

        int pos = GetPosition(letters, text[idx]);
        result += EncodeSymbol(pos, cumFreq, l, h, waitingBit);
        for (size_t index = 0; index <= pos; ++index) {
            ++cumFreq[index];
        }
        std::cout << "Current encoded sequence: " << result << std::endl << std::endl;
    }
    std::cout << "Encoding END symbol" << std::endl;
    std::cout << "Cummulative frequencies:";
        for (const int value : cumFreq) {
            std::cout << " " << value;
        }
        std::cout << std::endl << "WB " << waitingBit << std::endl;
    result += EncodeSymbol(letters.size() - 1, cumFreq, l, h, waitingBit);
    std::cout << "WB " << waitingBit << std::endl << result << std::endl;
    ++waitingBit;
    char resultBit = (h & (1 << (N - 1))) > 0 ? '1' : '0';
    result += resultBit;
    while (waitingBit) {
        result += (resultBit == '1') ? '0' : '1';
        --waitingBit;
    }
    return result;
}

int main() {
    std::string text;
    std::cin >> text;
    std::vector<char> letters = GetLetters(text);
    std::string code = Encode(text, letters);
    std::cout << std::endl << "Result: " << code << std::endl;
    return 0;
}