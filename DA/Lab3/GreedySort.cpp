#include "GreedySort.h"

TGreedySort::TGreedySort(unsigned int n): n(n), cnt(std::vector<unsigned int> (3,0)), nums(std::vector<unsigned int> (n)) {
}

void TGreedySort::GetNums() {
    for(int i = 0; i < n; ++i) {
        std::cin >> nums[i];
        cnt[nums[i] - 1]++;
    }

    onesEnd = cnt[0];
    twosEnd = onesEnd + cnt[1];
    threesEnd = n;
}

void TGreedySort::PrintNums() {
    for(int i = 0; i < n; ++i) {
        std::cout << nums[i] << " ";
    }
    std::cout << std::endl;
}

void TGreedySort::Loop(unsigned int& k, unsigned int& cond, unsigned int pos, unsigned int a, unsigned int b, unsigned int& answer) {
    for(int i = k; i < cond; ++i, ++k) {
        if(nums[i] == a) {
            nums[pos] = a;
            nums[i] = b;
            answer++;
            break;
        }
    }
}

int TGreedySort::NumsSort() {
    unsigned int answer = 0;

    for(unsigned int i = 0; i < onesEnd; ++i) {
        unsigned int twos2 = onesEnd;
        unsigned int threes2 = twosEnd;

        unsigned int twos3 = onesEnd;
        unsigned int threes3 = twosEnd;

        if(nums[i] == 2) {
            Loop(twos2,twosEnd,i,1,2,answer);
            if(twos2 >= twosEnd) {
                Loop(threes2,threesEnd,i,1,2,answer);
            }
        }

        if(nums[i] == 3) {
            Loop(threes3,threesEnd,i,1,3,answer);
            if(threes3 >= threesEnd) {
                Loop(twos3,twosEnd,i,1,3,answer);
            }
        }
    }

    for(unsigned int i = onesEnd; i < twosEnd; ++i) {
        unsigned int threes = twosEnd;
        if(nums[i] == 3) {
            Loop(threes,threesEnd,i,2,3,answer);
        }
    }
    return answer;
}
