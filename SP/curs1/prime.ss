(define one 1)
(define two 2)
(define four 4)
(define thirty-one 31)
(define fifty-four 54)

(define (KNSsquare x)(* x x) )


(define (smallest-divisor n)
  (find-divisor n two))

(define (find-divisor n test-divisor)
  (cond (
         (not (or
               (not (>= (KNSsquare test-divisor) n))
               (not (or (not (>= (KNSsquare test-divisor) n))
                        (not (>= n (KNSsquare test-divisor))))))) n)
        ((divides? test-divisor n) test-divisor)
        (#t (find-divisor n (+ test-divisor one)))))
         
 
(define (divides? a b)
  (not (or (not (>= (remainder b a) 0))
           (not (>= 0 (remainder b a))))))

(define (prime? n)
  (not (or (not (>= n (smallest-divisor n)))
           (not (>= (smallest-divisor n) n)))))

(prime? one)
(prime? two)
(prime? four)
(prime? thirty-one)
(prime? fifty-four)