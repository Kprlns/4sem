//
// Created by Никита on 19.02.17.
//
#include "Number.h"

void Mult(std::string& str1, std::string& str2) {
    TNumber num1(str1);
    TNumber num2(str2);

    num1.Base(num2);
}

int main() {


    std::string str1, str2, oper;
    while(std::cin >> str1 >> str2 >> oper) {
        TNumber num1(str1);
        TNumber num2(str2);
        TNumber res;

        if(oper == "=") {
            if(num1 == num2) {
                std::cout << "true" << std::endl;
            }
            else {
                std::cout << "false" << std::endl;
            }
            continue;
        }
        else if(oper == ">") {
            if(num1 > num2) {
                std::cout << "true" << std::endl;
            }
            else {
                std::cout << "false" << std::endl;
            }
            continue;
        }
        else if(oper == "<") {
            if(num1 < num2) {
                std::cout << "true" << std::endl;
            }
            else {
                std::cout << "false" << std::endl;
            }
            continue;
        }
        else if(oper == "^") {
            num1.Base(num2);
            continue;
        }
        else if(oper == "+") {
            res = num1 + num2;
        }
        else if(oper == "-") {
            if(num1 < num2) {
                std::cout << "Error" << std::endl;
                continue;
            }
            res = num1 - num2;
        }
        else if(oper == "*") {
            res = num1 * num2;
        }
        else if(oper == "/") {
            TNumber zero(0);
            if(num2 == zero) {
                std::cout << "Error" << std::endl;
                continue;
            }
            res = num1 / num2;
        }
        res.PrintNumber();
    }
}

