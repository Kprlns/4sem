(define (KNSsquare x)(* x x) )


(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (KNSsquare test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))


(define (find-divisor n test-divisor)
  (cond (
         (not (or
               (not (>= (KNSsquare test-divisor) n))
               (not (or (not (>= (KNSsquare test-divisor) n))
                        (not (>= n (KNSsquare test-divisor))))))) n)
        ((divides? test-divisor n) test-divisor)
        (#t (find-divisor n (+ test-divisor 1)))))
         
 
(define (divides? a b)
  (not (or (not (>= (remainder b a) 0))
           (not (>= 0 (remainder b a))))))

(define (prime? n)
  (not (or (not (>= n (smallest-divisor n)))
           (not (>= (smallest-divisor n) n)))))


(prime? 1)
(prime? 2)
(prime? 4)
(prime? 54)