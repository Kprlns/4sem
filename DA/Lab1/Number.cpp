//
// Created by Никита on 19.02.17.
//
#include "Number.h"
const size_t width = 4;
const char fill = '0';

int StrToInt(std::string& str, int lBound, int rBound) {
    int res = 0;
    for(int i = lBound; i <= rBound; i++) {
        res += str[i] - '0';
        if(i != rBound){
            res *= 10;
        }
    }
    return res;
}


TNumber::TNumber() {
    size = 0;

}
TNumber::TNumber(int num) {
    this->size = 0;
    if(!num) {
        this->number.push_back(0);
        this->size++;
    }
    else {
        while(num) {
            this->number.push_back(num % BASE);
            num /= BASE;
            this->size++;
        }
    }

}

TNumber::TNumber(unsigned int size) {
    this->size = 0;
    this->number.resize(size,0);
}

void TNumber::RemoveLeadingZeros() {

    while(size > 1) {
        if(this->number[size - 1] == 0) {
            this->size--;

        }
        else {
            break;
        }
    }
}

TNumber::TNumber(std::string& input) {
    this->size = 0;
    for(int i = input.length() - 1; i >= 0; ) {
        int lBound = std::max(0, i - 3);
        int cnt = StrToInt(input,lBound,i);
        number.push_back(cnt);
        this->size++;
        i = lBound - 1;
    }
}


void TNumber::PrintNumber() {
    std::cout << this->number[this->size - 1] << "";
    for(int i = this->size - 2; i >= 0 ; --i) {
        std::cout <<std::setw(width) <<std::setfill(fill) << this->number[i] << "";
    }
    std::cout << '\n';
}

bool TNumber::operator>(TNumber& other) {
    if(this->size > other.size) {
        return true;
    }
    else if(this->size < other.size) {
        return false;
    }

    for(int i = this->size - 1; i >= 0; --i) {
        if(this->number[i] > other.number[i]) {
            return true;
        }
        else if(this->number[i] < other.number[i]) {
            return false;
        }
    }
    return false;
}

bool TNumber::operator<(TNumber& other) {
    return other > *this;
}

bool TNumber::operator==(TNumber& other) {
    if(this->size != other.size) {
        return false;
    }
    for(int i = 0; i < this->size; ++i) {
        if(this->number[i] != other.number[i]) {
            return false;
        }
    }
    return true;
}

TNumber TNumber::operator+(TNumber& other) {
    unsigned int len = std::max(this->size,other.size);
    TNumber res(len + 1);
    res.size = len + 1;
    int k = 0;
    for(int i = 0; i < len; ++i) {
        if(i < this->size) {
            k += this->number[i];
        }
        if(i < other.size) {
            k += other.number[i];
        }

        res.number[i] += k % BASE;
        if(res.number[i] >= BASE) {
            int cnt = res.number[i] / BASE;
            res.number[i] %= BASE;
            res.number[i + 1] += cnt;
        }
        res.number[i + 1] += k / BASE;
        k = 0;
    }
    res.RemoveLeadingZeros();
    return res;
}

TNumber TNumber::operator-(TNumber& other) {

        int i = 0;
        for(i = 0; i < other.size; ++i) {
            this->number[i] -= other.number[i];
            if(this->number[i] < 0) {
                this->number[i + 1]--;
                this->number[i] += BASE;
            }
        }
        for(; i < this->size; ++i) {
            if(this->number[i] < 0) {
                this->number[i + 1]--;
                this->number[i] += BASE;
            }
            else {
                break;
            }
        }
        this->RemoveLeadingZeros();


    return *this;
}

TNumber TNumber::operator*(TNumber& other) {

    TNumber result(this->size + other.size);

    for(int i = 0; i < this->size; ++i) {
        if(number[i]) {
            int k = 0;
            for(int j = 0; j < other.size; ++j) {
                int t = this->number[i] * other.number[j] +
                        result.number[i + j] + k;
                result.number[i + j] = t % BASE;

                k = t / BASE;
            }
            result.number[i + other.size] = k;
        }
    }
    result.size = size + other.size;
    result.RemoveLeadingZeros();

    return result;
}


TNumber TNumber::operator/(TNumber& other) {
    TNumber res;
    if(other > *this) {
        res.number.push_back(0);
        res.size = 1;
        return res;
    }

    int d = BASE / (other.number[other.size - 1] + 1);

    TNumber dNumber(d);
    TNumber u,v;
    u = (*this) * dNumber;
    v = other * dNumber;

    res.size = u.size;
    res.number.resize(u.size,0);

    TNumber r;
    r.number.push_back(0);
    r.size = 0;

    TNumber base(BASE);
    for(int i = u.size - 1; i >= 0 ; --i) {
        r = r * base;
        TNumber u1(u.number[i]);
        r = r + u1;

        int c1,c2;
        if(r.size <= v.size) {
            c1 = 0;
        }
        else {
            c1 = r.number[v.size];
        }

        if(r.size <= v.size - 1) {
            c2 = 0;
        }

        else {
            c2 = r.number[v.size - 1];
        }

        int q = (c1 * BASE + c2) / v.number[v.size - 1];

        TNumber qNumber(q);
        TNumber sub = v * qNumber;
        while(sub > r) {
            q--;
            sub = sub - v;
        }
        r = r - sub;
        res.number[i] = q;

        this->size = left.size - right.size;
        this->RemoveLeadingZeros();

    }
    res.RemoveLeadingZeros();
    return res;
}

void TNumber::Base(TNumber& other) {
    if(other.size == 1 && !other.number[0]) {
        if(this->size == 1 && !this->number[0]) {
            std::cout << "Error\n";
        }
        else {
            std::cout << "1" << std::endl;
        }
        return;
    }
    else if(this->size == 1 && !this->number[0]){
        std::cout << "0" << std::endl;
        return;
    }

    TNumber res(1);
    TNumber one(1);
    TNumber two(2);
    while(!(other.size == 1 && !other.number[0])) {
        if(other.number[0] % 2) {
            other.number[0]--;
            res = res * (*this);
        }
        else {
            *this = (*this) * (*this);
            other = other / two;
        }
    }
    res.PrintNumber();

}

TNumber::~TNumber() {

}
