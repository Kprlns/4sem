import re

inputFile = open("../test100000", "r")
outputFile = open("check3", "w")

for string in inputFile:
    splittedString = string.split()
    a = int(splittedString[0])
    b = int(splittedString[1])
    if splittedString[2] == "+":
        outputFile.write(str(a + b) + "\n")

    if splittedString[2] == "-":
        outputFile.write(str(a - b) + "\n")

    if splittedString[2] == "*":
        outputFile.write(str(a * b) + "\n")

    if splittedString[2] == "/":
        outputFile.write(str(a // b) + "\n")

    if splittedString[2] == "^":
        outputFile.write(str(a ** b) + "\n")
