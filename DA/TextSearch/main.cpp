#include "Index.h"
#include "Load.h"
#include "Query.h"

int main(int argc, char* argv[]) {
    std::locale::global(std::locale("en_US.UTF-8") );
    std::wcin.imbue(std::locale());
    std::wcout.imbue(std::locale());
    if(std::string(argv[1]) == std::string("index")) {
        TIndex index;
        std::string inputName;
        std::string outputName;
        for(uint32_t i = 2; i < argc; i += 2) {
            if(std::string(argv[i]) == std::string("--input")) {
                inputName = std::string(argv[i + 1]);
            }
            else if( std::string(argv[i]) == std::string("--output")) {
                outputName = argv[i + 1];
            }
        }
        index.IndexAll(inputName);
        index.SaveNames(outputName);
        index.SaveIndex(outputName);
    }
    else if(std::string(argv[1]) == std::string("search")) {
        TQuery queries;
        std::string indexName;
        std::string inputName;
        std::string outputName;
        for(uint32_t i = 2; i < argc; ++i) {
            if(std::string(argv[i]) == std::string("--index")) {
                indexName = argv[i + 1];
                ++i;
            }
            else if(std::string(argv[i]) == std::string("--input")) {
                inputName = argv[i + 1];
                ++i;
            }
            else if(std::string(argv[i]) == std::string("--output")) {
                outputName = argv[i + 1];
                ++i;
            }
            else if(std::string(argv[i]) == std::string("--full-output")) {
                queries.ChangeMode();
            }
        }
        queries.LoadIndex(indexName);
        queries.Queries(inputName,outputName);
    }
}
