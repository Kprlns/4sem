#ifndef INC_4SEM_KUHN_H
#define INC_4SEM_KUHN_H

#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

class TKuhn {
public:

    TKuhn(int n);

    void GetEdges(int m);

    void FindSets();
    void PrintSets();
    void Kuhn(std::vector<int>& set1, std::vector<int>& set2);
    void PrintResult();

    void MaxMatching();


    bool Prolong(int v, std::vector<int>& matching, std::vector<bool>& used);


private:

    void Dfs(int v, std::vector<bool>& used, bool flag);

    int length;
    int n;
    int empty = 0;
    std::vector< std::vector<int> > edges;

    std::vector< std::pair<int,int> > result;

    std::vector < std::vector<int> > sets;
};

#endif //INC_4SEM_KUHN_H
