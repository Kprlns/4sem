//
// Created by Никита on 03.04.17.
//

#include "Index.h"

bool TIndex::GetName() {
    std::wstring str;

    std::getline(std::wcin, str);
    if(!str.size()) {
        return false;
    }


    uint64_t k = 0;
    uint64_t cnt = 0;
    uint64_t i = str.length() - 1;

    while(cnt != 2) {
        if(str[i] == '\"') {
            cnt++;
        }
        else if(cnt == 1) {
            ++k;
        }
        --i;
    }
    i += 2;

    names.push_back(str.substr(i,k));
    return true;

}

void TIndex::PrintNames() {
    for(uint64_t i = 0; i < names.size(); ++i) {
        std::wcout << names[i] << std::endl;
    }
}

void TIndex::Index(uint64_t n) {
    std::wstring str,tmp;
    std::string str1;
    std::set<std::wstring> words;
    bool flag = false;
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    do {
        std::getline(std::cin, str1);
        str = converter.from_bytes(str1);
        //std::getline()
        for(uint64_t i = 0; i < str.length(); ++i) {
            if(std::iswalpha(str[i]) || iswdigit(str[i])) {
                flag = true;
                tmp.push_back(str[i]);
            }
            else if(flag) {
                words.insert(tmp);
                flag = false;
                tmp.clear();
            }
        }

        if(flag) {
            words.insert(tmp);
            flag = false;
            tmp.clear();
        }
    } while(str.back() != '>');


    std::pair<std::map<std::wstring, std::vector<uint64_t> >::iterator , bool> ret;

    for(auto it : words) {
        //std:: << it << std::endl;
        ret = index.insert(std::make_pair(it,std::vector<uint64_t>())); //emplace
        ret.first->second.push_back(n);
    }

}

void TIndex::IndexAll() {
    uint64_t cnt = 1;
    while(GetName()) {
        Index(cnt);
        ++cnt;
    }
}

void TIndex::Print() {
    this->PrintNames();
    for(auto& it: this->index) {
        std::wcout << it.first <<  ": ";
        for(int i = 0; i < it.second.size(); ++i) {
            std::wcout << it.second[i];
        }
        std::wcout << '\n';
    }
}



void TIndex::Save() {

}