#include <iostream>
#include <vector>
#include <stack>

class TSubrectangle {
public:

    TSubrectangle(unsigned int n, unsigned int m);

    void GetMatrix();

    const void D1(std::vector<int>& d, std::vector<int>& d1);
    const void D2(std::vector<int>& d, std::vector<int>& d2);

    const int FindZeroSubrectangle();

private:

    unsigned int n;
    unsigned int m;
    std::vector<std::vector<int>> matrix;
};
