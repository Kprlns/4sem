/* $mlisp17 */
#include "semantics.h"
int tSM::p02(){ //    PROG -> CALCS1
	return 0;}
int tSM::p03(){ //    PROG -> DEFS
	return 0;}
int tSM::p04(){ //    PROG -> DEFS CALCS1
	return 0;}
int tSM::p05(){ //  CALCS1 -> CALCS
	return 0;}
int tSM::p06(){ //   CALCS -> CALC
	return 0;}
int tSM::p07(){ //   CALCS -> CALCS CALC
	return 0;}
int tSM::p08(){ //    CALC -> E1
	return 0;}
int tSM::p09(){ //    CALC -> BOOL
	return 0;}
int tSM::p10(){ //    CALC -> STR
	return 0;}
int tSM::p11(){ //    CALC -> SET
	return 0;}
int tSM::p12(){ //    CALC -> DISP
	return 0;}
int tSM::p13(){ //      E1 -> E
	return 0;}
int tSM::p15(){ //       E -> CONST
	return 0;}
int tSM::p16(){ //       E -> ADD
	return 0;}
int tSM::p17(){ //       E -> SUB
	return 0;}
int tSM::p18(){ //       E -> DIV
	return 0;}
int tSM::p19(){ //       E -> MUL
	return 0;}
int tSM::p20(){ //       E -> COND
	return 0;}
int tSM::p21(){ //       E -> IF
	return 0;}
int tSM::p22(){ //       E -> CPROC
	return 0;}
int tSM::p23(){ //   CONST -> $int
	return 0;}
int tSM::p24(){ //   CONST -> $zero
	return 0;}
int tSM::p25(){ //   CONST -> $float
	return 0;}
int tSM::p26(){ //     ADD -> HADD E1 )
	return 0;}
int tSM::p27(){ //    HADD -> ( +
	return 0;}
int tSM::p28(){ //    HADD -> HADD E1
	return 0;}
int tSM::p29(){ //     SUB -> HSUB E1 )
	return 0;}
int tSM::p30(){ //    HSUB -> ( -
	return 0;}
int tSM::p31(){ //    HSUB -> HSUB E1
	return 0;}
int tSM::p32(){ //     DIV -> HDIV E1 )
	return 0;}
int tSM::p33(){ //    HDIV -> ( /
	return 0;}
int tSM::p34(){ //    HDIV -> HDIV E1
	return 0;}
int tSM::p35(){ //     MUL -> HMUL E1 )
	return 0;}
int tSM::p36(){ //    HMUL -> ( *
	return 0;}
int tSM::p37(){ //    HMUL -> HMUL E1
	return 0;}
int tSM::p38(){ //    COND -> HCOND ELSE )
	return 0;}
int tSM::p39(){ //    COND -> HCOND CLAUS )
	return 0;}
int tSM::p40(){ //   HCOND -> ( cond
	return 0;}
int tSM::p41(){ //   HCOND -> HCOND CLAUS
	return 0;}
int tSM::p42(){ //   CLAUS -> HCLAUS E1 )
	return 0;}
int tSM::p43(){ //  HCLAUS -> ( BOOL
	return 0;}
int tSM::p44(){ //  HCLAUS -> HCLAUS SET
	return 0;}
int tSM::p45(){ //  HCLAUS -> HCLAUS DISP
	return 0;}
int tSM::p46(){ //    ELSE -> HELSE E1 )
	return 0;}
int tSM::p47(){ //   HELSE -> ( else
	return 0;}
int tSM::p48(){ //   HELSE -> HELSE SET
	return 0;}
int tSM::p49(){ //   HELSE -> HELSE DISP
	return 0;}
int tSM::p50(){ //      IF -> IFTRUE E1 )
	return 0;}
int tSM::p51(){ //  IFTRUE -> HIF E
	return 0;}
int tSM::p52(){ //     HIF -> ( if BOOL
	return 0;}
int tSM::p56(){ //    BOOL -> $bool
	return 0;}
int tSM::p57(){ //    BOOL -> CPRED
	return 0;}
int tSM::p58(){ //    BOOL -> REL
	return 0;}
int tSM::p59(){ //    BOOL -> OR
	return 0;}
int tSM::p60(){ //    BOOL -> AND
	return 0;}
int tSM::p61(){ //    BOOL -> ( not BOOL )
	return 0;}
int tSM::p65(){ //     REL -> HREL E1 )
	return 0;}
int tSM::p66(){ //    HREL -> ( < E
	return 0;}
int tSM::p67(){ //    HREL -> ( = E
	return 0;}
int tSM::p68(){ //    HREL -> ( > E
	return 0;}
int tSM::p69(){ //    HREL -> ( >= E
	return 0;}
int tSM::p70(){ //    HREL -> ( <= E
	return 0;}
int tSM::p71(){ //      OR -> HOR BOOL )
	return 0;}
int tSM::p72(){ //     HOR -> ( or
	return 0;}
int tSM::p73(){ //     HOR -> HOR BOOL
	return 0;}
int tSM::p74(){ //     AND -> HAND BOOL )
	return 0;}
int tSM::p75(){ //    HAND -> ( and
	return 0;}
int tSM::p76(){ //    HAND -> HAND BOOL
	return 0;}
int tSM::p77(){ //     STR -> $str
	return 0;}
int tSM::p78(){ //     STR -> SIF
	return 0;}
int tSM::p79(){ //     SIF -> SIFTRUE STR )
	return 0;}
int tSM::p80(){ // SIFTRUE -> HIF STR
	return 0;}
int tSM::p83(){ //    DISP -> ( display E1 )
	return 0;}
int tSM::p84(){ //    DISP -> ( display BOOL )
	return 0;}
int tSM::p85(){ //    DISP -> ( display STR )
	return 0;}
int tSM::p86(){ //    DISP -> ( newline )
	return 0;}
int tSM::p87(){ //    DEFS -> DEF
	return 0;}
int tSM::p88(){ //    DEFS -> DEFS DEF
	return 0;}
int tSM::p89(){ //     DEF -> PRED
	return 0;}
int tSM::p90(){ //     DEF -> VAR
	return 0;}
int tSM::p91(){ //     DEF -> PROC
	return 0;}
//_____________________
int tSM::p107(){return 0;} int tSM::p108(){return 0;} 
int tSM::p109(){return 0;} int tSM::p110(){return 0;} 


