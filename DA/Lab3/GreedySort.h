#include <iostream>
#include <vector>

class TGreedySort {
public:

    TGreedySort(unsigned int n);

    void GetNums();
    void PrintNums();

    void Loop(unsigned int& k, unsigned int& cond, unsigned int pos, unsigned int a, unsigned int b, unsigned int& answer);

    int NumsSort();

private:

    unsigned int n;

    unsigned int onesEnd;
    unsigned int twosEnd;
    unsigned int threesEnd;

    std::vector<unsigned int> cnt;
    std::vector<unsigned int> nums;
};