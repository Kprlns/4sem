/*** test

 китайский 中華人民共和 Japanese 日本 иврит עִבְרִית что-то непонятное Եբրայերեն խոսում են

***/
#include <iostream>
#include <string>
#include <clocale>
#include <locale>
#include <codecvt>

;

int main() {
    setlocale(LC_ALL, "");
    std::ios::sync_with_stdio(false);
    std::locale::global(std::locale("ru_RU.UTF-8") );
    std::wcin.imbue(std::locale());
    std::wcout.imbue(std::locale());

    std::wstring text, tmp;

    std::getline(std::wcin, text);
    //text = L"китайский 中華人民共和 Japanese 日本 иврит עִבְרִית что-то непонятное Եբրայերեն խոսում են";

    for(int i = 0; i < text.size(); ++i) {
        if(isalnum(text[i], std::locale())) {
            tmp.push_back(text[i]);
        }
        else {
            std::wcout << tmp << std::endl;
            tmp.clear();
        }
    }
}