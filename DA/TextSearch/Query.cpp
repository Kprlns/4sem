#ifndef TEXTSEARCH2_CPP
#define TEXTSEARCH2_CPP

#include "Query.h"

void TQuery::LoadIndex(std::string &inputName) {
    index.Load(inputName);
}

void TQuery::Negate(std::vector <uint32_t>& list, std::vector<uint32_t>& dest) {
    uint32_t size = index.NamesSize();
    std::vector<uint32_t> res;

    uint32_t j = 0;
    for(uint32_t i = 0; i < size; ++i) {
        if(j == list.size() || i < list[j]) {
            res.push_back(i);
        }
        else if(i == list[j]) {
            j++;
        }
    }

    dest.swap(res);
}

void TQuery::Negate(std::vector<uint32_t>& list) {
    Negate(list, list);
}


void TQuery::Union(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2, std::vector<uint32_t>& dest) {
    std::vector<uint32_t> res;
    std::set_union(list1.begin(),list1.end(),list2.begin(),list2.end(),std::back_inserter(res));

    dest.swap(res);
}

void TQuery::Union(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2) {
    Union(list1, list2, list1);
}


void TQuery::Intersection(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2, std::vector<uint32_t>& dest) {
    std::vector<uint32_t> res;
    std::set_intersection(list1.begin(),list1.end(),list2.begin(),list2.end(),std::back_inserter(res));
    dest.swap(res);
}

void TQuery::Intersection(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2) {
    Intersection(list1,list2,list1);
}

void TQuery::GetList(std::wstring& word, std::vector<uint32_t>& dest) {
    auto&& iter = index.index.find(word);
    if(iter == index.index.end()) {
        dest = std::vector<uint32_t > (0);
        return;
    }
    dest = iter->second;
}




bool TQuery::isOperation(wchar_t c) {
    return (c == L'&') || (c == L'|') || (c == L'~');
}

uint32_t TQuery::Priority(wchar_t oper) {
    if(oper == L'|') {
        return 1;
    }
    else if (oper == L'&') {
        return 2;
    }
    else if(oper == L'~') {
        return 3;
    }
    else {
        return 0;
    }
}


void TQuery::PerformOperation(wchar_t op) {
    if(op == L'~') {
        std::vector<uint32_t>& tmp = stack.back();
        std::vector<uint32_t> res;
        Negate(tmp);
    }
    else if(op == L'|') {
        std::vector<uint32_t>& op1 = stack[stackSize - 1];
        std::vector<uint32_t>& op2 = stack[stackSize - 2];
        Union(op1,op2,op2);
        stack.pop_back();
        stackSize--;
    }
    else if(op == L'&') {
        std::vector<uint32_t>& op1 = stack[stackSize - 1];
        std::vector<uint32_t>& op2 = stack[stackSize - 2];
        Intersection(op1,op2,op2);
        stack.pop_back();
        stackSize--;
    }
}



void TQuery::Queries(std::string& inputFile, std::string& outputFile) {
    std::wstring query;
    std::wifstream input(inputFile.c_str());
    std::wofstream output(outputFile.c_str());
    while(std::getline(input, query)) {
        PerformQuery(query);

        std::vector<uint32_t>& res = stack[stackSize - 1];
        output << res.size() << L"\n";
        if(fullOutput) {
            for(uint32_t i = 0; i < res.size(); ++i) {
                output << index.names[res[i]] << L"\n";
            }

        }
        stack.pop_back();
        stackSize--;
    }
    input.close();
    output.close();
}

void TQuery::PerformQuery(std::wstring& query1) {

    std::wstringstream query;
    query << query1;
    std::wstring str;
    while(query >> str) {

        if(str == L"(") {
            operations.push(L'(');
        }
        else if(str == L")") {
            while(operations.top() != L'(') {
                PerformOperation(operations.top());
                operations.pop();
            }
            operations.pop();
        }
        else if(isOperation(str[0])) {
            wchar_t op = str[0];
            bool unary = (op == L'~');
            while(!operations.empty() &&
                    ((!unary && (Priority(operations.top()) >= Priority(op)))
                    || (unary && (Priority(operations.top()) > Priority(op)))))
            {
                PerformOperation(operations.top());
                operations.pop();
            }
            operations.push(op);
        }
        else {
            for(uint32_t i = 0; i < str.length(); ++i) {
                str[i] = std::tolower(str[i],std::locale());
            }
            std::vector<uint32_t> vector;
            GetList(str,vector);
            stack.emplace_back(vector);
            stackSize++;
        }
    }

    while(!operations.empty()) {
        PerformOperation(operations.top());
        operations.pop();
    }
}

void TQuery::ChangeMode() {
    fullOutput = true;
}
#endif
