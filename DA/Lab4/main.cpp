#include "Kuhn.h"

int main() {
    int a,b;
    std::cin >> a >> b;
    TKuhn kuhn(a);
    kuhn.GetEdges(b);
    kuhn.FindSets();
    //kuhn.PrintSets();

    //kuhn.Kuhn();
    kuhn.MaxMatching();
    kuhn.PrintResult();
}