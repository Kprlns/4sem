#ifndef TEXTSEARCH1_CPP
#define TEXTSEARCH1_CPP
#include "Load.h"

void TLoad::LoadNames(std::string& inputName) {
    std::wifstream namesIn( (inputName + ".names").c_str(),std::ios_base::binary);
    while(!namesIn.eof()) {
        std::wstring str;
        std::getline(namesIn,str);
        if(namesIn.eof()) {
            break;
        }
        names.push_back(str);
        str.clear();
    }
    namesIn.close();
}

void TLoad::LoadIndex(std::string& inputName) {
    std::wifstream inWords( (inputName + ".index1").c_str(),std::ios_base::binary);
    std::wifstream inNumbers( (inputName + ".index2").c_str(),std::ios_base::binary);
    uint32_t size;
    while(!inWords.eof()) {
        std::wstring str;
        str.clear();
        std::getline(inWords,str);
        if(inWords.eof()) {
            break;
        }

        std::vector<uint32_t> vec;
        inNumbers >> size;
        uint32_t n;
        for(uint32_t i = 0; i < size; ++i) {
            inNumbers >> n;
            vec.push_back(n);
        }

        index.insert(std::make_pair(str,vec));
        if(inWords.eof()) {
            break;
        }
    }
    inWords.close();
    inNumbers.close();
}

void TLoad::Load(std::string& inputName) {
    LoadNames(inputName);
    LoadIndex(inputName);
}

void TLoad::Print() {
    this->PrintNames();
    for(auto& it: this->index) {
        std::wcout << it.first <<  ": ";
        for(int32_t i = 0; i < it.second.size(); ++i) {
            std::wcout << it.second[i] << " ";
        }
        std::wcout << '\n';
    }
}

void TLoad::PrintNames() {
    for (uint32_t i = 0; i < names.size(); ++i) {
        std::wcout << names[i] << std::endl;
    }
}

uint32_t TLoad::NamesSize() {
    return (uint32_t)names.size();
}
#endif
