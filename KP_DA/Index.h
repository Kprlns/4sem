#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <list>
#include <set>
#include <clocale>
#include <locale>
#include <codecvt>
//
// Created by Никита on 03.04.17.
//

#ifndef OOP_TEXTSEARCH_H
#define OOP_TEXTSEARCH_H

class TIndex {
public:

    bool GetName();

    void PrintNames();
    void Index(uint64_t n);
    void IndexAll();
    void Print();
    void Save();



private:



    std::vector<std::wstring> names;
    std::map<std::wstring, std::vector<uint64_t> > index;

};

#endif //OOP_TEXTSEARCH_H
