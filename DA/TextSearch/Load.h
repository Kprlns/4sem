#include <vector>
#include <string>
#include <set>
#include <clocale>
#include <locale>
#include <iostream>
#include <fstream>
#include <ostream>
#include <unordered_map>
#include <sstream>
#include <cinttypes>
#ifndef TEXTSEARCH1_H
#define TEXTSEARCH1_H
class TQuery;

class TLoad{
public:
    uint32_t NamesSize();
    void PrintNames();
    void Load(std::string& inputName);
    void Print();

    friend class TQuery;

private:

	void LoadNames(std::string& inputName);
    void LoadIndex(std::string& inputName);
    
    std::vector<std::wstring> names;
    std::unordered_map<std::wstring, std::vector<uint32_t> > index;

};
#endif
