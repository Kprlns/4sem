#include <algorithm>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <string>
#include <vector>

const int N = 4;

inline void PrintValueAndBits(int value, const std::string& name) {
    std::cout << name << ": " << value << ", bits: ";
    int currentPower = 1 << (N - 1);
    while (currentPower) {
        std::cout << (value & currentPower ? '1' : '0');
        currentPower >>= 1;
    }
    std::cout << std::endl;
}

std::vector<char> GetLetters(const std::string& text) {
    std::set<char> tmp;
    for (size_t idx = 0; idx < text.size(); ++idx) {
        tmp.insert(text[idx]);
    }
    tmp.insert(std::numeric_limits<char>::max());
    std::vector<char> result;
    for (const auto& item : tmp) {
        result.push_back(item);
    }
    return result;
}

int GetPosition(const std::vector<char>& letters, const char letter) {
    return std::find(letters.begin(), letters.end(), letter) - letters.begin();
}

int DecodeSymbol(const std::string& text, const std::vector<int>& cumFreq, int& l, int& h, int& value) {
    static int pos = N;
    std::cout << "Processed symbols: " << pos << std::endl;
    int cum = ((value - l + 1) * cumFreq[0] - 1) / (h - l + 1);
    std::cout << "Cummulative value: " << cum << std::endl;
    int idx = 1;
    while (cumFreq[idx] > cum) {
        ++idx;
    }
    std::cout << "Symbol index: " << idx << std::endl;
    int lNew = l + (h - l + 1) * cumFreq[idx] / cumFreq[0];
    int hNew = l + (h - l + 1) * cumFreq[idx - 1] / cumFreq[0] - 1;
    l = lNew;
    h = hNew;
    PrintValueAndBits(l, "l");
    PrintValueAndBits(h, "h");
    PrintValueAndBits(value, "value");

    int lastBit = 1 << (N - 1);
    while (((h & lastBit) == (l & lastBit)) || (h - l < cumFreq[0])) {
        if ((h & lastBit) == (l & lastBit)) {
            if (l >= lastBit) {
                l -= lastBit;
                h -= lastBit;
            }
            if (value >= lastBit) {
                value -= lastBit;
            }
            l = l << 1;
            h = (h << 1) + 1;
            value = (value << 1);
            if (pos < text.size()) {
                value += (text[pos++] - '0');
            }
            PrintValueAndBits(l, "Update l");
            PrintValueAndBits(h, "Update h");
            PrintValueAndBits(value, "Update value");
        } else {
            std::cout << "Waiting bit" << std::endl;
            int tmpValue = 1 << (N - 2);
            l = 2 * (l - tmpValue);
            h = 2 * (h - tmpValue) + 1;
            value = 2 * (value - tmpValue);
            if (pos < text.size()) {
                value += (text[pos++] - '0');
            }
            PrintValueAndBits(l, "Update l");
            PrintValueAndBits(h, "Update h");
            PrintValueAndBits(value, "Update value");
        }
    }
    return idx;
}

std::string Decode(const std::string& text, const std::vector<char>& letters) {
    int l = 0;
    int h = (1 << N) - 1;
    PrintValueAndBits(l, "Starting value of l");
    PrintValueAndBits(h, "Starting value of h");
    std::cout << std::endl;
    std::vector<int> cumFreq(letters.size() + 1);
    std::string result;

    for (size_t idx = 0; idx < cumFreq.size(); ++idx) {
        cumFreq[idx] = cumFreq.size() - 1 - idx;
    }

    int value = 0;
    for (size_t idx = 0; idx < N; ++idx) {
        value = (value << 1) + (text[idx] - '0');
    }
    int decodedPos = 0;
    while (decodedPos != letters.size()) {
        std::cout << "Cummulative frequencies:";
        for (const int tmpValue : cumFreq) {
            std::cout << " " << tmpValue;
        }
        std::cout << std::endl;
        decodedPos = DecodeSymbol(text, cumFreq, l, h, value);
        for (size_t idx = 0; idx < decodedPos; ++idx) {
            ++cumFreq[idx];
        }
        if (decodedPos != letters.size()) {
            std::cout << "Decoded symbol: " << letters[decodedPos - 1] << std::endl << std::endl;
            result += letters[decodedPos - 1];
        }
    }

    return result;
}

int main() {
    std::string text;
    std::cin >> text; // Letters in first line
    std::vector<char> letters = GetLetters(text);
    std::cin >> text; // encoded text
    std::string code = Decode(text, letters);
    std::cout << std::endl << "Result: " << code << std::endl;
    return 0;
}