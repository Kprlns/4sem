//               xxxfloat.cpp
#include <iostream>
#include <iomanip>
#include "fsm.h"
using namespace std;

int main()
{
/*  tFSM Afloat;
///////////////////////
// построить автомат
  addrange(Afloat,0,'0','9',1);
  addrange(Afloat,1,'0','9',1);
  addstr(Afloat,1,".",2);
  addrange(Afloat,2,'0','9',2);
//......................
 Afloat.final(2);
//......................
///////////////////////
  cout << "*** xxx Afloat "
       << "size=" << Afloat.size()
       << " ***\n";
  cout << endl;

  while(true)
 {
  char input[81];
  cout << ">";
  cin.getline(input,81);
  if(!*input) break;
  int res = Afloat.apply(input);
  cout << setw(res?res+1:0) << "^"
       << endl;
 }
 return 0;*/
    tFSM Afloat;

    addstr(Afloat,0,"+-",0);
    addrange(Afloat,0,'0','9',0);
    addstr(Afloat,0,".",1);
    addrange(Afloat,1,'0','9',1);
    addstr(Afloat,1,"eE",2);
    addstr(Afloat,2,"+-",2);
    addrange(Afloat,2,'0','9',3);
    Afloat.final(3);

    cout << "*** xxx Afloat "
         << "size=" << Afloat.size()
         << " ***\n";
    cout << endl;

    while(true)
    {
        char input[81];
        cout << ">";
        cin.getline(input,81);
        if(!*input) break;
        int res = Afloat.apply(input);
        cout << setw(res?res+1:0) << "^"
             << endl;
    }
    return 0;

}

