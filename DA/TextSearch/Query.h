#include <iostream>
#include <string>
#include <stack>
#include <algorithm>
#include "Load.h"

#ifndef TEXTSEARCH2_H
#define TEXTSEARCH2_H
class TQuery {
public:

    void ChangeMode();

    void LoadIndex(std::string& inputName);
    void Queries(std::string& inputFile, std::string& outputFile);


private:

    void Negate(std::vector<uint32_t>& list, std::vector<uint32_t>& dest);
    void Negate(std::vector<uint32_t>& list);

    void Union(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2, std::vector<uint32_t>& dest);
    void Union(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2);

    void Intersection(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2, std::vector<uint32_t>& dest);
    void Intersection(std::vector<uint32_t>& list1, std::vector<uint32_t>& list2);

    void GetList(std::wstring& word, std::vector<uint32_t >& dest);

    uint32_t Priority(wchar_t str);
    bool isOperation(wchar_t str);

    void PerformQuery(std::wstring& query1);
    void PerformOperation(wchar_t op);

    TLoad index;
    
    uint32_t stackSize = 0;
	bool fullOutput = false;
    
    std::stack< wchar_t > operations;
    std::vector< std::vector<uint32_t> > stack;
};
#endif
