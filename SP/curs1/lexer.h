//                 lexer.h 2017
#ifndef LEXER_H
#define LEXER_H
#include "baselexer.h"
//********************************************
//*        Developed by  xxx		     *
//*             (c)  2017                    *
//********************************************
class tLexer:public tBaseLexer{
public:
// ������������ ��� ������������
 std::string Authentication()const{
                     return "KNS"
                +std::string("6");}
//�����������
 tLexer():tBaseLexer()
    {
//������� ��������:

//  ���� Azero
        addstr(Azero, 0, "+-", 2);
        addstr(Azero, 0, "0", 1);
        addstr(Azero, 2, "0", 1);
        addstr(Azero, 1, "0", 1);
        Azero.final(1);

//________________________________________

// ����� �����
        addstr(Aint, 0, "+-", 2);
        addstr(Aint, 0, "0", 2);
        addstr(Aint, 2, "0", 2);
        addrange(Aint, 0, '1', '9',1);
        addrange(Aint, 2, '1', '9', 1);
        addrange(Aint, 1, '0', '9' ,1);
        Aint.final(1);
//________________________________________

// ������������
        /*
        addstr(Afloat, 0, "+-", 0);
        addrange(Afloat, 0, '0', '9', 0);
        addstr(Afloat, 0, ".", 1);
        addrange(Afloat, 1, '0', '9', 1);
        addstr(Afloat, 1, "eE", 2);
        addstr(Afloat, 2, "+-", 2);
        addrange(Afloat, 2, '0', '9', 3);
        Afloat.final(3);
         */
        //addstr(Afloat,0,"+-",0);
        addstr(Afloat,0,"+-",6);
        addrange(Afloat,0,'0','9',1);
        addrange(Afloat,6,'0','9',1);

        addrange(Afloat,1,'0','9',1);
        addstr(Afloat,1,".",2);
        addrange(Afloat,2,'0','9',3);
        addrange(Afloat,3,'0','9',3);
        addstr(Afloat,3,"eE",4);
        addstr(Afloat,4,"+-",7);

        addrange(Afloat,7,'0','9',5);

        addrange(Afloat,4,'0','9',5);
        addrange(Afloat,5,'0','9',5);
        Afloat.final(5);
//________________________________________

// ������
//  ����������� escape-������������������:
// \'  \"  \?  \\  \a  \b  \f  \n  \r  \t  \v
        const char SP = ' ';// ������
        addstr(Astr, 0, "\"", 1);
        addstr(Astr, 1, "\"", 2);
        addrange(Astr, 1, SP, '!', 1);

        addstr(Astr, 1, "\\", 3);
        addstr(Astr, 3, "'?\\abfnrtv\"", 1);

        addrange(Astr, 1, '#', '[', 1);
        addrange(Astr, 1, ']', '~', 1);

        addrange(Astr, 1, '\x80', '\xff', 1);// ������� �����
        Astr.final(2);
//________________________________________

// �������������
        /*
         addstr(Aid, 0, "-", 0);
        addstr(Aid, 0, "!", 1);
        addrange(Aid,0,'0','9',3);

        addrange(Aid, 3, '0', '9' ,3);
        addrange(Aid, 3, 'a', 'z', 1);
        addrange(Aid, 3, 'A', 'Z', 1);
        addstr(Aid, 3, "-" ,1);
        addstr(Aid, 3, "!" ,1);

        addrange(Aid, 0, 'a', 'z', 1);
        addrange(Aid, 0, 'A', 'Z', 1);

        addrange(Aid, 1, 'a', 'z', 1);
        addrange(Aid, 1, 'A', 'Z', 1);
        addrange(Aid, 1, '0', '9', 1);

        addstr(Aid, 1, "!-", 1);
         */
        addrange(Aid, 0, 'a', 'z', 1);
        addrange(Aid, 0, 'A', 'Z', 1);
        addstr(Aid, 0, "-", 2);
        addstr(Aid, 0, "!", 1);

        addrange(Aid, 2, '0', '9' ,2);
        addstr(Aid, 2, "-", 2);

        addrange(Aid, 2, 'a', 'z', 1);
        addrange(Aid, 2, 'A', 'Z', 1);
        addstr(Aid, 2, "!", 1);

        addrange(Aid, 1, 'a', 'z', 1);
        addrange(Aid, 1, 'A', 'Z', 1);
        addrange(Aid, 1, '0', '9', 1);
        addstr(Aid, 1, "!-", 1);
        Aid.final(1);
//________________________________________

// ������������� ���������
        //addstr(Aidq, 0, "?", 1);
        addstr(Aidq, 0, "-", 3);
        addstr(Aidq, 0, "?", 2);
        addrange(Aidq, 0, 'a', 'z', 1);
        addrange(Aidq, 0, 'A', 'Z', 1);

        addrange(Aidq, 3, '0', '9' ,1);
        addrange(Aidq, 3, 'a', 'z', 1);
        addrange(Aidq, 3, 'A', 'Z', 1);
        addstr(Aidq, 3, "-", 3);

        addstr(Aidq, 1, "-", 3);

        addrange(Aidq, 1, 'a', 'z', 1);
        addrange(Aidq, 1, 'A', 'Z', 1);
        addrange(Aidq, 1, '0', '9', 1);

        addstr(Aidq, 1, "?", 2);
        addrange(Aidq, 2, 'a', 'z', 1);
        addrange(Aidq, 2, 'A', 'Z', 1);
        addrange(Aidq, 2, '0', '9', 1);
        addstr(Aidq, 2, "-", 1);
        addstr(Aidq, 2, "?" ,2);

        Aidq.final(2);
//________________________________________

// ��������
        addstr(Aoper, 0, "+-*/=", 1);
        addstr(Aoper, 0,"><",2);
        addstr(Aoper, 2, "=",1);
        Aoper.final(1);
        Aoper.final(2);
//________________________________________

// ��������� ���������
        addstr(Abool, 0, "#", 1);
        addstr(Abool, 1, "tf", 2);
        Abool.final(2);
//________________________________________
    }
};
#endif

