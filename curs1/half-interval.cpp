/*  KNS6   */
#include "mlisp.h"
double half__interval__metod(double a, double b);
double __KNS6__try(double neg__point, double pos__point);
bool close__enough_Q(double x, double y);
double average(double x, double y);
double fun(double z);
double disp(double a);
double root(double a, double b);
//________________ 
double one = 1.;
double tolerance = 0.00001e0;
double half__interval__metod(double a, double b){{
  double a__value = 0;
  double b__value = 0;
  a__value = fun(a);
  b__value = fun(b);
  return (!(!(!(!(0>=a__value)||!(!(0>=a__value)||!(a__value>=0))))||!(!(!(b__value>=0)||!(!(b__value>=0)||!(0>=b__value))))) ? __KNS6__try(a, b) : !(!(!(!(a__value>=0)||!(!(a__value>=0)||!(0>=a__value))))||!(!(!(0>=b__value)||!(!(0>=b__value)||!(b__value>=0))))) ? __KNS6__try(b, a) : true ? (b + one) : _infinity);
}}
double __KNS6__try(double neg__point, double pos__point){{
  double midpoint = 0;
  double test__value = 0;
  midpoint = average(neg__point, pos__point);
  display("+");
  return (close__enough_Q(neg__point, pos__point) ? midpoint : true ? test__value = fun(midpoint), (!(!(test__value>=0)||!(!(test__value>=0)||!(0>=test__value))) ? __KNS6__try(neg__point, midpoint) : !(!(0>=test__value)||!(!(0>=test__value)||!(test__value>=0))) ? __KNS6__try(midpoint, pos__point) : true ? midpoint : _infinity) : _infinity);
}}
bool close__enough_Q(double x, double y){{
  return !(!(tolerance>=abs((x + ( - y))))||!(!(tolerance>=abs((x + ( - y))))||!(abs((x + ( - y)))>=tolerance)));
}}
double half = 5.0e-1;
double two = 2.;
double average(double x, double y){{
  return (x + y) * half;
}}
double twelve = 12.;
double thirteen = 13.;
double fun(double z){{
  z = (z + ( - twelve * ( 1. / thirteen)) + ( - ( 1. / pi)));
  return (two * sin(z) * cos(z) + ( - half));
}}
double disp(double a){{
  return a;
}}
double root(double a, double b){{
  double temp = 0;
  temp = half__interval__metod(a, b);
  newline();
  display("interval=\t[");
  display(a);
  display(" , ");
  display(b);
  display("]\n");
  display("discrepancy=\t");
  display(fun(temp));
  newline();
  display("root=\t\t");
  return (!(!((temp + ( - b) + ( - one))>=0)||!(!((temp + ( - b) + ( - one))>=0)||!(0>=(temp + ( - b) + ( - one))))) ? display("[bad]"), disp(temp) : true ? display("[good]"), disp(temp) : _infinity);
}}
double neg__one = -1.;
int main(){
 display(" KNS variant 12"); newline();
 display(root(neg__one, 0)); newline();
 std::cin.get();
 return 0;
}

