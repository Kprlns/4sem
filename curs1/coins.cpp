/*  KNS6   */
#include "mlisp.h"
double GR__AMOUNT();
double count__change(double amount);
double cc(double amount, double largest__coin);
double next__coin(double coin);
//________________ 
double one = 1.;
double two = 2.;
double three = 3.;
double five = 5.;
double ten = 10.;
double hundred = 100.;
double LAGEST__COIN = 10.;
double LAST__DIGIT__OF__GROUP__NUMBER = 8.;
double GR__AMOUNT(){{
  return ten * LAST__DIGIT__OF__GROUP__NUMBER;
}}
double count__change(double amount){{
  return cc(amount, LAGEST__COIN);
}}
double cc(double amount, double largest__coin){{
  return (!(!(amount>=0)||!(0>=amount))||!(!(largest__coin>=one)||!(one>=largest__coin)) ? one : 0>=amount||0>=largest__coin ? 0 : true ? (cc(amount, next__coin(largest__coin)) + cc((amount + ( - largest__coin)), largest__coin)) : _infinity);
}}
double next__coin(double coin){{
  return (!(!(coin>=ten)||!(ten>=coin)) ? five : !(!(coin>=five)||!(five>=coin)) ? three : !(!(coin>=three)||!(three>=coin)) ? two : !(!(coin>=two)||!(two>=coin)) ? one : true ? 0 : _infinity);
}}
int main(){
display(" KNS variant 6");
newline();
display(" 1-2-3-5-10");
newline();
display("count-change for 100 \t= ");
display(count__change(hundred));
newline();
display("count-change for ");
display(GR__AMOUNT());
display(" \t= ");
display(count__change(GR__AMOUNT()));
newline();
 std::cin.get();
 return 0;
}

