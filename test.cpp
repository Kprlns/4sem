#include <iostream>
#include <vector>
#include <stack>

void fun(std::stack< std::vector<int> >& st) {
{
std::vector<int> v1(1,1);
std::vector<int> v2(2,2);
std::vector<int> v3(3,3);

st.emplace(v1);
st.emplace(v2);
st.emplace(v3);
}

}



int main() {

std::stack< std::vector<int> > stack;
fun(stack);

while(!stack.empty()) {
std::vector<int>& v = stack.top();
for(int i = 0; i < v.size(); i++) {
	std::cout << v[i] << " ";
}
std::cout << std::endl;
stack.pop();
}
}