#include "Kuhn.h"

TKuhn::TKuhn(int n) :edges(std::vector< std::vector <int> >(n,std::vector<int>(0))),
                     sets(std::vector< std::vector<int> >(2,std::vector<int>(0))) {
    this->n = n;
    length = 0;
}

void TKuhn::GetEdges(int m) {
    int first,second;
    for(int i = 0; i < m; ++i) {
        std::cin >> first >> second;
        first--;
        second--;
        edges[first].push_back(second);
        edges[second].push_back(first);
    }

    for(int i = 0; i < n; ++i) {
        if(edges[i].size()) {
            std::sort(edges[i].begin(), edges[i].end());
        }
        else {
            empty++;
        }
    }
}

void TKuhn::Dfs(int v, std::vector<bool>& used, bool flag) {
    if(used[v]) {
        return;
    }

    used[v] = true;
    sets[flag].push_back(v);

    for(int i = 0; i < edges[v].size(); ++i) {
        if(!used[ edges[v][i] ]) {
            Dfs(edges[v][i],used,!flag);
        }
    }
}

void TKuhn::FindSets() {
    std::vector<bool> used(n, false);

    for(int i = 0; i < n; ++i) {
        if(!used[i] && edges[i].size()) {
            Dfs(i, used, false);
        }
    }
    std::sort(sets[0].begin(),sets[0].end());
    std::sort(sets[1].begin(),sets[1].end());

}

void TKuhn::PrintSets() {
    std::sort(sets[0].begin(),sets[0].end());
    std::sort(sets[1].begin(),sets[1].end());


    for(int j = 0; j < sets.size(); ++j) {
        std::cout << "set" << j + 1 << ": ";
        for(int i = 0; i < sets[j].size(); ++i) {
            std::cout << sets[j][i] + 1 << " ";
        }
        std::cout << '\n';
    }
}



bool TKuhn::Prolong(int v, std::vector<int>& matching, std::vector<bool>& used) {
    if(used[v]) {
        return false;
    }
    else {
        used[v] = true;

        for(int i = 0; i < edges[v].size(); ++i) {
            int tmp = edges[v][i];
            if(matching[tmp] == -1 || Prolong(matching[tmp],matching,used)) {
                matching[tmp] = v;
                return true;
            }
        }
    }
    return false;
}


void TKuhn::Kuhn(std::vector<int>& set1, std::vector<int>& set2) {
    std::vector<int> matching(n,-1);
    std::vector<bool> used(n,false);

    for(int i = 0; i < set1.size(); ++i) {
        int j = set1[i];
        used.assign(n, false);
        Prolong(j,matching,used);
    }

    //for(int i = 0; i < n; ++i) {
    //    std::cout << i << " " << matching[i] << "\n";
    //}

    for(int i = 0; i < set2.size(); ++i) {
        int j = set2[i];
        if(matching[j] != - 1) {
            length++;
            result.push_back( std::make_pair(std::min(j,matching[j]),
                                             std::max(j,matching[j]) ));
            //matching[matching[i]] = -1;
        }
    }
}

void TKuhn::MaxMatching() {
    if(sets[0][0] < sets[1][0]) {
        Kuhn(sets[0], sets[1]);
    }
    else {
        Kuhn(sets[1],sets[0]);
    }
}

void TKuhn::PrintResult() {
    std::sort(result.begin(),result.end());

    std::cout << length << '\n';

    for(int i = 0; i < result.size(); ++i) {
        std::cout << result[i].first + 1 << " " << result[i].second + 1 << '\n';
    }
}