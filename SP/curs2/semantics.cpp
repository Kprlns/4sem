/* $mlisp17 */
#include "semantics.h"
#include "semempty.cpp"
using namespace std;
void tSM::init(){
  globals.clear();
  locals.clear();
  params.clear();
  scope =0; // ���������� �������
//���������:
  globals["e"] = 
//          properties
    tgName(VAR|DEFINED|BUILT);
  globals["pi"] = 
    tgName(VAR|DEFINED|BUILT);
//
// ������������ ���������:
  globals["remainder"] =
//          properties       arity
    tgName(PROC|DEFINED|BUILT, 2);
  globals["abs"] =
    tgName(PROC|DEFINED|BUILT, 1);
// ...
// ������ �� ���������, ������� ������������
// � ����� �������� ������������ �2
	return;}
int tSM::p01(){ //       S -> PROG
 bool error=false;
 for(tGlobal::iterator it=globals.begin();
     it!=globals.end();
     ++it){
// ����������� ������� ���������� ����
// � � ��������� �� ������� ������� �����
// ���� ���������, �� �� ������������ ��������,
// � ����� ��������������, �� �� ������������
// ����������. ��������� �������� [!].
// ����� ����, ������������ ��� ���� ������������,
// �� �� �������������� ���������� � ����������,
// �� ����������� ����������. ��������� �������� [?].
//   it->first   ���
//   it->second  ������� ������
// ...
 }//for...
 if(error) return 1;
	return 0;}
int tSM::p14(){ //       E -> $id
//????????????????
// ����������� �������� �������
// ���������� ����������

	return 0;}
int tSM::p53(){ //   CPROC -> HCPROC )
 string name = S1->name;
 int    count= S1->count;
 if(scope!=0){// ������ ���������
    if(locals.count(name)){// ���������
//p53-1.ss
      ferror_message="[!]Procedure application:"
        " local variable '"+name+
        "' hides the procedure!";
      return 1;
    } // if locals ...
    if(params.count(name)){// ��������
//p53-2.ss
      ferror_message="[!]Procedure application:"
        " parameter '"+name+
        "' hides the procedure!";
      return 1;
    }// if params...
 }// if scope... 
 do{
//  ����� ��� � ���������� �������
    tgName& ref = globals[name];
    if(ref.empty()){//����������� ���
       if(scope==0){// ��� ���������
//p53-3.ss
        ferror_message="[!]Procedure application:"
          " procedure '"+ name+
          "' is not defined !";
        return 1;
       }//if scope
// ������ ���������
//        ������� ����� ������� ������
      ref = tgName(PROC|USED, count);
      break;
     }

// ��� �������
    if(!ref.test(PROC)){//�� ���������
//p53-4.ss
      ferror_message="[!]Procedure application:"
        "  '"+ name+
        "' is not a procedure!";
      return 1;
    }

    if(ref.arity!=count){//����� ����������
//                �� ����� ����� ���������� 
     std::ostringstream buf;
     buf<<"[!]Procedure application: '"<<name<< "' "
//p53-5.ss
        <<(ref.test(DEFINED) ? "expects " // ���������
//                                      ��� ����������
//p53-6.ss

// ��������� ��� �� ����������, �� ��� ���������� �����         
               : "has been called already\n\t with "
          )
        <<ref.arity<<" argument"
        <<(ref.arity!=1 ?"s":"")
        <<", given: "<<count<<" !";
     ferror_message = buf.str();
     return 1;
    }
// ������ ���
    ref.set(USED);//��� ������������
   }while(false);

	return 0;}
int tSM::p54(){ //  HCPROC -> ( $id
 S1->name = S2->name;
 S1->count = 0;
	return 0;}
int tSM::p55(){ //  HCPROC -> HCPROC E
 ++S1->count;
	return 0;}
int tSM::p62(){ //   CPRED -> HCPRED )
	return 0;}
int tSM::p63(){ //  HCPRED -> ( $idq
	return 0;}
int tSM::p64(){ //  HCPRED -> HCPRED E
	return 0;}
int tSM::p81(){ //     SET -> HSET E1 )
	return 0;}
int tSM::p82(){ //    HSET -> ( set! $id
	return 0;}
int tSM::p92(){ //    PRED -> HPRED BOOL )
	return 0;}
int tSM::p93(){ //   HPRED -> PDPAR )
	return 0;}
int tSM::p94(){ //   PDPAR -> ( define ( $idq
	return 0;}
int tSM::p95(){ //   PDPAR -> PDPAR $id
	return 0;}
int tSM::p96(){ //     VAR -> VARINI )
	return 0;}
int tSM::p97(){ //  VARINI -> HVAR CONST
	return 0;}
int tSM::p98(){ //    HVAR -> ( define $id
	return 0;}
int tSM::p99(){ //    PROC -> PRBODY )
 params.clear();
 locals.clear();
 scope = 0;
	return 0;}
int tSM::p100(){ //  PRBODY -> HPROC E
	return 0;}
int tSM::p101(){ //   HPROC -> PCPAR )
//????????????????
// ����������� �������� ������� 
// ����������� ���������
 S1->count = 0;
	return 0;}
int tSM::p102(){ //   HPROC -> HPROC VAR
	return 0;}
int tSM::p103(){ //   HPROC -> HPROC SET
	return 0;}
int tSM::p104(){ //   HPROC -> HPROC DISP
	return 0;}
int tSM::p105(){ //   PCPAR -> ( define ( $id
 S1->name = S4->name;
 S1->count = 0;
 scope = 1;
	return 0;}
int tSM::p106(){ //   PCPAR -> PCPAR $id
    if(params.count(S2->name)){
//p106-1.ss
      ferror_message=
        "[!]Procedure definition: in '"
        +S1->name+
        "' duplicate parameter identifier '"
        +S2->name+"'!";
      return 1;
    }
    params.insert(S2->name);
 ++S1->count;
	return 0;}
//_____________________
