/*  KNS6   */
#include "mlisp.h"
double KNSsquare(double x);
double smallest__divisor(double n);
double find__divisor(double n, double test__divisor);
bool divides_Q(double a, double b);
bool prime_Q(double n);
//________________ 
double one = 1.;
double two = 2.;
double four = 4.;
double thirty__one = 31.;
double fifty__four = 54.;
double KNSsquare(double x){{
  return x * x;
}}
double smallest__divisor(double n){{
  return find__divisor(n, two);
}}
double find__divisor(double n, double test__divisor){{
  return (!(!(KNSsquare(test__divisor)>=n)||!(!(KNSsquare(test__divisor)>=n)||!(n>=KNSsquare(test__divisor)))) ? n : divides_Q(test__divisor, n) ? test__divisor : true ? find__divisor(n, (test__divisor + one)) : _infinity);
}}
bool divides_Q(double a, double b){{
  return !(!(remainder(b, a)>=0)||!(0>=remainder(b, a)));
}}
bool prime_Q(double n){{
  return !(!(n>=smallest__divisor(n))||!(smallest__divisor(n)>=n));
}}
int main(){
 display(prime_Q(one)); newline();
 display(prime_Q(two)); newline();
 display(prime_Q(four)); newline();
 display(prime_Q(thirty__one)); newline();
 display(prime_Q(fifty__four)); newline();
 std::cin.get();
 return 0;
}

