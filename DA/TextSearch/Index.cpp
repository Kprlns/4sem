#ifndef TEXTSEARCH_CPP
#define TEXTSEARCH_CPP
#include "Index.h"

bool TIndex::GetName(std::wifstream& input) {
    std::wstring str;

    while(!str.size()) {
        if(input.eof()) {
            return false;
        }
        std::getline(input, str);
    }
    uint32_t k = 0;
    uint32_t cnt = 0;
    uint32_t i = str.length() - 1;

    while(cnt != 2) {
        if(str[i] == '\"') {
            cnt++;
        }
        else if(cnt == 1) {
            ++k;
        }
        --i;
    }
    i += 2;

    names.push_back(str.substr(i,k));
    return true;

}

void TIndex::PrintNames() {
    for(uint32_t i = 0; i < names.size(); ++i) {
        std::wcout << names[i] << std::endl;
    }
}

void TIndex::Index(uint32_t n, std::wifstream& input) {
    std::wstring str,tmp;
    std::unordered_set<std::wstring> words;
    std::wstring end = L"</doc>";

    bool flag = false;
    do {
        std::getline(input, str);
        if(str == end) {
            break;
        }
        for(uint32_t i = 0; i < str.length(); ++i) {
            if(isalnum(str[i], std::locale())){
                flag = true;
                tmp.push_back(std::tolower(str[i],std::locale()));
            }
            else if(flag) {
                words.insert(tmp);
                flag = false;
                tmp.clear();
            }
        }
        if(flag) {
            words.insert(tmp);
            flag = false;
            tmp.clear();
        }
    } while(str != end);

    for(auto& it : words) {
        auto&& ret = index.insert(std::make_pair(it,std::vector<uint32_t>()));
        ret.first->second.push_back(n);
    }
}

void TIndex::IndexAll(std::string& inputName) {
    uint32_t cnt = 1;

    std::wifstream input((inputName).c_str());
    while(GetName(input)) {
        Index(cnt, input);
        ++cnt;
    }
    input.close();
}

void TIndex::Print() {
    this->PrintNames();
    for(auto& it: this->index) {
        std::wcout << it.first <<  ": ";
        for(uint32_t i = 0; i < it.second.size(); ++i) {
            std::wcout << it.second[i] << " ";
        }
        std::wcout << '\n';
    }
}



void TIndex::SaveNames(std::string& outputName) {
    std::wofstream namesOut( (outputName + ".names").c_str(),std::ios_base::binary);
    if(namesOut.is_open()) {
        for (uint32_t i = 0; i < names.size(); ++i) {
            namesOut << names[i] << L"\n";
        }
    }
    namesOut.close();
}

void TIndex::SaveIndex(std::string& outputName) {
    std::wofstream outWords((outputName + ".index1").c_str(),std::ios_base::binary);
    std::wofstream outNumbers( (outputName + ".index2").c_str(), std::ios_base::binary);


    if(outWords.is_open() && outNumbers.is_open()) {
        for(auto& it: index) {
            outWords << it.first << L"\n";
            outNumbers << it.second.size() << L" ";
            for(uint32_t i = 0; i < it.second.size(); ++i) {
                outNumbers << it.second[i] - 1 << L" ";
            }
        }
    }
    outNumbers.close();
    outWords.close();
}
#endif
