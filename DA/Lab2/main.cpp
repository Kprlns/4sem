#include "LargestSubrectangle.h"

int main() {

    unsigned int n,m;
    std::cin >> n >> m;

    TSubrectangle matrix(n,m);
    matrix.GetMatrix();
    std::cout << matrix.FindZeroSubrectangle() << std::endl;
}