import random
n = int(input())

f = open("test100000", "w")
res = open("res100000", "w")

op = ["+", "-", "*", "/", "^"]

i = 0
while i < n:
    var1 = random.randint(10000000000000000000000000000000, 100000000000000000000000000000000000000000000)
    var2 = random.randint(10000000000000000000, 1000000000000000000000)
    if var1 < var2:
        continue
    else:
        cnt = random.randint(0, 3)
        f.write(str(var1)+" "+str(var2)+" "+op[cnt]+'\n')

        if cnt == 0:
            res.write(str(var1 + var2) + '\n')

        if cnt == 1:
            res.write(str(var1 - var2) + '\n')

        if cnt == 2:
            res.write(str(var1 * var2) + '\n')

        if cnt == 3:
            res.write(str(var1 // var2) + '\n')

        i+=1
