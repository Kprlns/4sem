#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <clocale>
#include <locale>
#include <fstream>
#include <ostream>
#include <unordered_set>
#include <cinttypes>
#ifndef TEXTSEARCH_H
#define TEXTSEARCH_H

class TIndex {
public:

    bool GetName(std::wifstream& input);

    void IndexAll(std::string& inputName);
    void SaveNames(std::string& outputName);
    void SaveIndex(std::string& outputName);

    void Print();
    void PrintNames();
private:

    void Index(uint32_t n, std::wifstream& input);

    std::vector<std::wstring> names;
    std::unordered_map<std::wstring, std::vector<uint32_t> > index;

};

#endif
