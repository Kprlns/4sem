#include "LargestSubrectangle.h"

TSubrectangle::TSubrectangle(unsigned int n,unsigned  int m) : n(n), m(m), matrix(std::vector<std::vector<int>> (n, std::vector<int> (m))) {

}

void TSubrectangle::GetMatrix() {
    char c;
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < m; ++j) {
            std::cin >> c;
            matrix[i][j] = c - '0';
        }
    }
    /*
    std::cout << "\n";
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < m; ++j) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << "\n";
    }
     */
}

const void TSubrectangle::D1(std::vector<int>& d, std::vector<int>& d1) {
    std::stack<int> stack;
    for(int i = 0; i < m; ++i) {
        while(!stack.empty() && d[stack.top()] <= d[i]) {
            stack.pop();
        }

        if(stack.empty()) {
            d1[i] = -1;
        }
        else {
            d1[i] = stack.top();
        }
        stack.push(i);
    }
}

const void TSubrectangle::D2(std::vector<int>& d, std::vector<int>& d2) {
    std::stack<int> stack;
    for(int i = m - 1; i >= 0; --i) {
        while(!stack.empty() && d[stack.top()] <= d[i]) {
            stack.pop();
        }

        if(stack.empty()) {
            d2[i] = m;
        }
        else {
            d2[i] = stack.top();
        }
        stack.push(i);
    }

}

const int TSubrectangle::FindZeroSubrectangle() {
    std::vector<int> d(m, -1);
    std::vector<int> d1(m, 0);
    std::vector<int> d2(m, 0);

    int answer = 0;

    for(int i = 0; i < n; ++i) {

        for(int j = 0; j < m; ++j) {
            if(matrix[i][j] == 1) {
                d[j] = i;
            }
        }

        D1(d,d1);
        D2(d,d2);

        for(int j = 0; j < m; ++j) {
            int tmp = (i - d[j]) * (d2[j] - d1[j] - 1);
            //std::cout << tmp << " ";
            answer = std::max(answer, tmp);
        }
        //std::cout << '\n';
    }

    //std::cout << '\n';
    return answer;
}
