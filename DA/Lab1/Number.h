//
// Created by Никита on 19.02.17.
//
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <stack>
#ifndef NUMBER_H
#define NUMBER_H

const int BASE = 10000;

class TNumber {

public:
    TNumber();
    TNumber(int num);
    TNumber(unsigned int size);
    TNumber(std::string& input);

    //void operator =(TNumber& other);
    TNumber operator +(TNumber& other);
    TNumber operator -(TNumber& other);
    TNumber operator *(TNumber& other);
    TNumber operator /(TNumber& other);

    void Base( TNumber& other);

    bool operator>(TNumber& other);
    bool operator<(TNumber& other);
    bool operator==(TNumber& other);

    //TNumber(TNumber& left, TNumber& right, bool flag);
    //void Del(TNumber& right, TNumber& left);
    void PrintNumber();
    void RemoveLeadingZeros();



    ~TNumber();
private:
    std::vector<int> number;
    unsigned int size;

};


#endif //OOP_NUMBER_H
